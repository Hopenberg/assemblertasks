.686
.model flat

public _sortowanie

;unsigned __int64 sortowanie(unsigned __int64 * tabl, unsigned int n);

.code
_sortowanie PROC
	push ebp
	mov ebp, esp
	sub esp, 4
	push esi
	push edi
	push ebx

	mov esi, [ebp + 8]	; adres tablicy
;	mov [ebp - 4], esi
	mov ecx, [ebp + 12]	; rozmiar tablicy

ptl1:
	mov ebx, 0	; indeks przeszukiwania
	dec ecx
	push ecx	; zachowaj ta wartosc

ptl2:
	mov eax, [esi + 8*ebx]	; mlodsze
	mov edx, [esi + 8*ebx + 4]	; starsze

; najpierw porownaj mlodsze czesci
	cmp eax, dword ptr [esi + 8*ebx + 8]
	jb dalej
	ja zamien
	cmp edx, dword ptr [esi + 8*ebx + 12]
	jbe dalej
zamien:
	mov edi, [esi + 8*ebx + 8]	; mlodsza czesc
	mov [esi + 8*ebx], edi
	mov edi, [esi + 8*ebx + 12]	; starsza czesc
	mov [esi + 8*ebx + 4], edi
	mov [esi + 8*ebx + 8], eax	; mlodsza
	mov [esi + 8*ebx + 12], edx	; starsza
dalej:
	inc ebx
	loop ptl2

	pop ecx
	cmp ecx,1
	jne ptl1

	mov ecx, [ebp + 12]
	dec ecx ; do indeksowania od 0
	mov eax, [esi + 8*ecx]
	mov edx, [esi + 8*ecx + 4]


	pop ebx
	pop edi
	pop esi
	add esp, 4
	pop ebp
	ret
_sortowanie ENDP
END