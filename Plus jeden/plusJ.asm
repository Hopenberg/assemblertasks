.686
.model flat

public _plus_jeden
extern _malloc : PROC

.code
_plus_jeden PROC
	push ebp
	mov ebp, esp
	push ebx
	push esi
	push edi
; odczytanie liczby
; w formacie double
	mov eax, [ebp+8]
	mov edx, [ebp+12]
; wpisanie 1 na pozycji o wadze 2^0
; mantysy do EDI:ESI
	mov esi, 0
	mov edi, 00100000H
; wyodr�bnienie pola
; wyk�adnika (11-bitowy)
; bit znaku liczby z za�o�enia = 0
	mov ebx, edx
	shr ebx, 20
; obliczenie pierwotnego wyk�adnika
; pot�gi
	sub ebx, 1023
; zerowanie wyk�adnika i bitu znaku
	and edx, 000FFFFFH
; dopisanie niejawnej jedynki
	or edx, 00100000H

	mov cl, bl
	shr edi, cl	; przesun notacje jedynki w prawo, do wykladnika liczby zrodlowej
	add edx, edi	; dodaj 1
	bt edx, 21	; sprawdz czy zwiekszyla sie dlugosc liczby
	jnc dalej

	inc ebx		; zwieksz wykladnik
	shr edx, 1	; przesun edx o 1 w prawo
	rcr eax, 1	; tak samo eax, ale przez cf

dalej:
	add ebx, 1023

	and edx, 000FFFFFH	; miejsce na wykladnik
	shl ebx, 20
	or edx, ebx	; nalozenie wykladnika do edx
	bt dword ptr [ebp+12], 31	; sprawdzenie bitu znaku
	jnc koniec

	bts edx, 31
koniec:

; za�adowanie obliczonej warto�ci z
; EDX:EAX na wierzcho�ek stosu
; koprocesora
	push edx
	push eax
	fld qword PTR [esp]
	add esp, 8
	pop edi
	pop esi
	pop ebx
	pop ebp
	ret
_plus_jeden ENDP
END