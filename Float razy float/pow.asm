.686
.model flat

public _float_razy_float



.code
_przesunEDXEAX_o_ECX_p PROC
	push ecx
ptl:
	shr edx, 1
	rcr eax, 1
	loop ptl

	pop ecx
	ret
_przesunEDXEAX_o_ECX_p ENDP

_float_razy_float PROC
	push ebp
	mov ebp, esp
	push esi
	push edi
	push ebx


; w dh wykladnik ebx 'a', w dl wykladnik 'b'

	mov eax, [ebp + 8]	; float a
	mov ebx, [ebp + 12]	; float b

	and ebx, 007FFFFFH
	and eax, 007FFFFFH
	bts ebx, 23
	bts eax, 23	; ustawienie niejawnych jedynek

	mov edx, 0
	mul ebx	; mnozenie eax * ebx -> a * b, wynik w edx:eax

	mov ecx, 23
	call _przesunEDXEAX_o_ECX_p


	push edx
	push eax	; aby wykorzystac te rejsetry
	mov eax, [ebp + 8]	; float a
	mov ebx, [ebp + 12]	; float b

	shr ebx, 23	; wydobycie wykladnika
	shr eax, 23
	mov dh, bl
	mov dl, al

	sub dh, 127
	sub dl, 127
	add dh, dl
	add dh, 127	; wykladnik gotowy

	movzx ecx, dh
	pop eax
	pop edx
; w ECX wykladnik
	btr eax, 24	; sprawdzenie 24 bitu, ewentualne wyzerowanie go
	jnc scal

	inc ecx		; zwieksz wykladnik
	shr eax, 1	; przesun mantyse o 1 w prawo
scal:
	btr eax, 23
	shl ecx, 23
	or eax, ecx

; dodanie bitu znaku
	mov ecx, [ebp + 12]
	mov edx, [ebp + 8]
	xor ecx, edx
	bt ecx, 31
	jnc wpisz

	bts eax, 31

wpisz:
	finit
	push eax
	fld dword ptr [esp]
	pop eax

	pop ebx
	pop edi
	pop esi
	pop ebp
	ret
_float_razy_float ENDP
END