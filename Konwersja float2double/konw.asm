.686
.model flat

public _konwersja
extern _malloc : PROC

.code
_konwersja PROC
	push ebp
	mov ebp, esp
	push esi
	push edi
	push ebx

	mov esi, [ebp + 8]	; liczba float
	mov edi, [ebp + 12]	; adres double

	and esi, 80000000H
	xor ebx, ebx ; w ebx starsza czesci double
	or ebx, esi

	mov esi, [ebp + 8]
	shl esi, 1
	shr esi, 24	; w ESI wykladnik
	sub esi, 127
	add esi, 1023	; wykladnik w double
	shl esi, 20		; ustawienie maski wykaldnika
	or ebx, esi		; wykladnik i bit znaku na miejscu

	mov esi, [ebp + 8]
	shl esi, 9
	shr esi, 12		; maska dla starszej czesci mantysy
	or ebx, esi		; starsza czesc double gotowa

	mov [edi + 4], ebx

	xor ebx, ebx
	mov esi, [ebp + 8]
	and esi, 7H
	shl esi, 29
	or ebx, esi
	mov [edi], ebx


	pop ebx
	pop edi
	pop esi
	pop ebp
	ret
_konwersja ENDP
END