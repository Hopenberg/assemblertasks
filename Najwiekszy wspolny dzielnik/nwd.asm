.686
.model flat

public _nwd
public _obj
extern __write : PROC
extern _malloc : PROC

;wchar_t * ASCI_na_UTF16(char * znaki, int n);

.code
_nwd PROC
	push ebp
	mov ebp, esp
	push esi
	push edi
	push ebx

	mov ebx, [ebp + 8]
	mov edx, [ebp + 12]

	cmp ebx, edx
	ja a_wieksze
	jb b_wieksze

	mov eax, ebx
	jmp koniec
a_wieksze:
	sub ebx, edx
	push edx
	push ebx
	call _nwd
	add esp, 8
	jmp koniec
b_wieksze:
	sub edx, ebx
	push edx
	push ebx
	call _nwd
	add esp, 8
	jmp koniec
koniec:

	pop ebx
	pop edi
	pop esi
	pop ebp
	ret
_nwd ENDP

_obj PROC
	push ebp
	mov ebp, esp
	sub esp, 4
	push esi
	push edi
	push ebx

	finit
	mov dword ptr [ebp - 4], 3
	fld dword ptr [ebp + 16]
	fld dword ptr [ebp + 12]
	fld dword ptr [ebp + 8]
	fld st(1)
	fmul st(0), st(0)
	fld st(2)
	fmul st(0), st(0)
	faddp
	fld st(1)
	fmul st(0), st(3)
	faddp
	fmul st(0), st(3)
	fldpi
	fmulp
	fild dword ptr [ebp - 4]
	fdivp
	fstp st(1)
	fstp st(1)
	fstp st(1)

	pop ebx
	pop edi
	pop esi
	add esp,4
	pop ebp
	ret
_obj ENDP
END