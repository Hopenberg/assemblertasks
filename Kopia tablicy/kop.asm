.686
.model flat

public _kopia_tablicy
extern _malloc : PROC

.code
_kopia_tablicy PROC
	push ebp
	mov ebp, esp
	push esi
	push edi
	push ebx

	mov esi, [ebp + 8]	; adres tablicy
	mov ecx, [ebp + 12]	; rozmiar tablicy

	shl ecx, 2
	push ecx
	call _malloc
	pop ecx		; w EAX wynik operacji malloc
	mov ecx, [ebp + 12]	; rozmiar tablicy

	cmp eax, 0
	jz koniec

;	mov edi, eax	; skopiuj adres nowej tablicy

	mov edi, 0	; indeks dla zrodla
	mov edx, 0	; indeks dla tablicy wynikowej

ptl1:
	bt dword ptr [esi + 4*edi], 0
	jc	dalej	; w carry wartosc pierwszego bitu

	mov ebx, [esi + 4*edi]
	mov [eax + 4*edx], ebx
	inc edx

dalej:
	inc edi
	loop ptl1

; reszte wypelnij zerami
	mov ecx,  [ebp + 12]
	sub ecx, edx
ptl2:
	mov dword ptr [eax + 4*edx], 0
	inc edx
	loop ptl2

koniec:

	pop ebx
	pop edi
	pop esi
	pop ebp
	ret
_kopia_tablicy ENDP
END