#include <stdio.h>

int * kopia_tablicy(int tabl[],	unsigned int n);


int main()
{
	int tabl[] = { 1,2,3,4,5,6,7,8,9,10 };
	int rozmiar = sizeof(tabl) / sizeof(int);
	int *kopia = kopia_tablicy(tabl, rozmiar);

	for (int i = 0; i < rozmiar; i++)
	{
		printf("%d ", kopia[i]);
	}

	return 0;
}